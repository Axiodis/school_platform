namespace SchoolPlatform.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AveragesTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Averages",
                c => new
                    {
                        AverageId = c.Int(nullable: false, identity: true),
                        Value = c.Int(nullable: false),
                        Year = c.Int(nullable: false),
                        Semester = c.Int(nullable: false),
                        StudentId = c.Int(nullable: false),
                        SubjectId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AverageId)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: false)
                .ForeignKey("dbo.Students", t => t.StudentId, cascadeDelete: true)
                .ForeignKey("dbo.Subjects", t => t.SubjectId, cascadeDelete: false)
                .Index(t => t.StudentId)
                .Index(t => t.SubjectId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Averages", "SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.Averages", "StudentId", "dbo.Students");
            DropForeignKey("dbo.Averages", "UserId", "dbo.Users");
            DropIndex("dbo.Averages", new[] { "UserId" });
            DropIndex("dbo.Averages", new[] { "SubjectId" });
            DropIndex("dbo.Averages", new[] { "StudentId" });
            DropTable("dbo.Averages");
        }
    }
}
