namespace SchoolPlatform.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MarksAndAbsences : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Absences",
                c => new
                    {
                        AbsenceId = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Semester = c.Int(nullable: false),
                        StudentId = c.Int(nullable: false),
                        SubjectId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AbsenceId)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: false)
                .ForeignKey("dbo.Students", t => t.StudentId, cascadeDelete: true)
                .ForeignKey("dbo.Subjects", t => t.SubjectId, cascadeDelete: true)
                .Index(t => t.StudentId)
                .Index(t => t.SubjectId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Marks",
                c => new
                    {
                        MarkId = c.Int(nullable: false, identity: true),
                        Value = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Semester = c.Int(nullable: false),
                        StudentId = c.Int(nullable: false),
                        SubjectId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MarkId)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: false)
                .ForeignKey("dbo.Students", t => t.StudentId, cascadeDelete: true)
                .ForeignKey("dbo.Subjects", t => t.SubjectId, cascadeDelete: true)
                .Index(t => t.StudentId)
                .Index(t => t.SubjectId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.SubjectClasses",
                c => new
                    {
                        SubjectClassId = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        SubjectId = c.Int(nullable: false),
                        ClassId = c.Int(nullable: false),
                        IsTeza = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.SubjectClassId)
                .ForeignKey("dbo.Classes", t => t.ClassId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.Subjects", t => t.SubjectId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.SubjectId)
                .Index(t => t.ClassId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SubjectClasses", "SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.SubjectClasses", "UserId", "dbo.Users");
            DropForeignKey("dbo.SubjectClasses", "ClassId", "dbo.Classes");
            DropForeignKey("dbo.Marks", "SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.Marks", "StudentId", "dbo.Students");
            DropForeignKey("dbo.Marks", "UserId", "dbo.Users");
            DropForeignKey("dbo.Absences", "SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.Absences", "StudentId", "dbo.Students");
            DropForeignKey("dbo.Absences", "UserId", "dbo.Users");
            DropIndex("dbo.SubjectClasses", new[] { "ClassId" });
            DropIndex("dbo.SubjectClasses", new[] { "SubjectId" });
            DropIndex("dbo.SubjectClasses", new[] { "UserId" });
            DropIndex("dbo.Marks", new[] { "UserId" });
            DropIndex("dbo.Marks", new[] { "SubjectId" });
            DropIndex("dbo.Marks", new[] { "StudentId" });
            DropIndex("dbo.Absences", new[] { "UserId" });
            DropIndex("dbo.Absences", new[] { "SubjectId" });
            DropIndex("dbo.Absences", new[] { "StudentId" });
            DropTable("dbo.SubjectClasses");
            DropTable("dbo.Marks");
            DropTable("dbo.Absences");
        }
    }
}
