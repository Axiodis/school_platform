namespace SchoolPlatform.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ClassTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                    "dbo.Classes",
                    c => new
                    {
                        ClassId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ClassId);

        }
        
        public override void Down()
        {
            DropTable("dbo.Classes");
        }
    }
}
