// <auto-generated />
namespace SchoolPlatform.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class MarksAndAbsences : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(MarksAndAbsences));
        
        string IMigrationMetadata.Id
        {
            get { return "201706011132289_MarksAndAbsences"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
