namespace SchoolPlatform.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProfessorsSubjects : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SubjectUsers",
                c => new
                    {
                        Subject_SubjectId = c.Int(nullable: false),
                        User_UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Subject_SubjectId, t.User_UserId })
                .ForeignKey("dbo.Subjects", t => t.Subject_SubjectId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.User_UserId, cascadeDelete: true)
                .Index(t => t.Subject_SubjectId)
                .Index(t => t.User_UserId);
            
            AddColumn("dbo.Classes", "UserId", c => c.Int(nullable: false));
            CreateIndex("dbo.Classes", "UserId", unique: true);
            AddForeignKey("dbo.Classes", "UserId", "dbo.Users", "UserId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Classes", "UserId", "dbo.Users");
            DropForeignKey("dbo.SubjectUsers", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.SubjectUsers", "Subject_SubjectId", "dbo.Subjects");
            DropIndex("dbo.SubjectUsers", new[] { "User_UserId" });
            DropIndex("dbo.SubjectUsers", new[] { "Subject_SubjectId" });
            DropIndex("dbo.Classes", new[] { "UserId" });
            DropColumn("dbo.Classes", "UserId");
            DropTable("dbo.SubjectUsers");
        }
    }
}
