namespace SchoolPlatform.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Mig3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Marks", "IsTeza", c => c.Boolean(nullable: false));
            AddColumn("dbo.SubjectClasses", "HasTeza", c => c.Boolean(nullable: false));
            DropColumn("dbo.SubjectClasses", "IsTeza");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SubjectClasses", "IsTeza", c => c.Boolean(nullable: false));
            DropColumn("dbo.SubjectClasses", "HasTeza");
            DropColumn("dbo.Marks", "IsTeza");
        }
    }
}
