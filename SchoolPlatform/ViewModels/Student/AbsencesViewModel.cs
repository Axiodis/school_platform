﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using SchoolPlatform.Models;
using SchoolPlatform.ViewModels.Commands;

namespace SchoolPlatform.ViewModels.Student
{
    public class AbsencesViewModel : BaseViewModel
    {
        public ObservableCollection<Absence> Absences { get; set; }
        public AbsencesViewModel()
        {
            Absences = new ObservableCollection<Absence>(Db.Absences
                .Where(a => a.Student.UserId == LoggedUser.UserId && a.Semester == Semester && a.Date.Year == Now.Year)
                .ToList());
        }


        private ICommand _backCommand;
        public ICommand BackCommand => _backCommand ?? (_backCommand = new RelayCommand(GoBack));

        public void GoBack(object param)
        {
            Application.Current.MainWindow.DataContext = new MainViewModel();
        }
    }
}
