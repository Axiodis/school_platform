﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using SchoolPlatform.Models;
using SchoolPlatform.ViewModels.Commands;

namespace SchoolPlatform.ViewModels.Student
{
    public class MarksViewModel : BaseViewModel
    {
        public ObservableCollection<Mark> Marks { get; set; }

        public MarksViewModel()
        {
            Marks = new ObservableCollection<Mark>(Db.Marks.Where(m => m.Student.UserId == LoggedUser.UserId && m.Semester == Semester && m.Date.Year == Now.Year).ToList());
        }

        private ICommand _backCommand;
        public ICommand BackCommand => _backCommand ?? (_backCommand = new RelayCommand(GoBack));

        public void GoBack(object param)
        {
            Application.Current.MainWindow.DataContext = new MainViewModel();
        }
    }
}
