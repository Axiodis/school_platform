﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using SchoolPlatform.Models;
using SchoolPlatform.ViewModels.Commands;

namespace SchoolPlatform.ViewModels
{
    public  class LogInViewModel : BaseViewModel
    {
        public string Username { get; set; }
        
        private ICommand _logInCommand;
        public ICommand LogInCommand => _logInCommand ?? (_logInCommand = new RelayCommand(LogIn));

        public void LogIn(object param)
        {
            PasswordBox passwordBox = param as PasswordBox;
            User user = null;
            using (var db = new SchoolPlatformContext())
            {

                try
                {
                    user = db.Users.First(u => u.Username.Equals(Username));
                }
                catch (Exception)
                {
                    MessageBox.Show("Invalid username!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            if (user != null && user.Password.Equals(passwordBox?.Password))
            {
                switch (user.Type)
                {
                    case UserType.Admin:
                        Application.Current.MainWindow.DataContext = new Admin.MainViewModel();
                        break;

                    case UserType.Professor:
                        Application.Current.MainWindow.DataContext = new Professor.MainViewModel();
                        break;
                    case UserType.Student:
                        Application.Current.MainWindow.DataContext = new Student.MainViewModel();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                
                LoggedUser = user;
            }
            else
            {
                MessageBox.Show("Invalid credentials!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
