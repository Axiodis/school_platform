﻿using System.Windows;
using SchoolPlatform.ViewModels.Admin;

namespace SchoolPlatform.ViewModels.Operations.AdminOperations
{
    public class AdminMainOperations
    {
        private readonly MainViewModel _mainViewModel;

        public AdminMainOperations(MainViewModel mainViewModel)
        {
            _mainViewModel = mainViewModel;
        }

        public void Students(object param)
        {
            Application.Current.MainWindow.DataContext = new Users();
        }

        public void Classes(object param)
        {
            Application.Current.MainWindow.DataContext = new Classes();
        }

        public void Subjects(object param)
        {
            Application.Current.MainWindow.DataContext = new Subjects();
        }

        public void SubjectsClasses(object param)
        {
            Application.Current.MainWindow.DataContext = new SubjectClassViewModel();
        }

        public void Exit(object param)
        {
            BaseViewModel.LoggedUser = null;
            Application.Current.MainWindow.DataContext = new LogInViewModel();
        }
    }
}
