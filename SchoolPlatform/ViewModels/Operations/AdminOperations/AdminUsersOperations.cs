﻿using System.Linq;
using System.Windows;
using SchoolPlatform.Models;
using SchoolPlatform.ViewModels.Admin;

namespace SchoolPlatform.ViewModels.Operations.AdminOperations
{
    public class AdminUsersOperations
    {
        public Users Users { get; }

        public AdminUsersOperations(Users users)
        {
            Users = users;
        }

        public void GoBack(object param)
        {
            Application.Current.MainWindow.DataContext = new MainViewModel();
        }

        public void AddUser(object param)
        {
            Application.Current.MainWindow.DataContext = new AddUser();
        }

        public void SaveChanges(object param)
        {
            Users.Db.SaveChanges();
        }

        public void Delete(object param)
        {
            if (Users.SelectedUser.Type == UserType.Professor && Users.Db.Classes.Count(c => c.UserId == Users.SelectedUser.UserId) > 0)
            {
                MessageBox.Show("You can't delete a class master!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Users.Db.Users.Remove(Users.SelectedUser);
            Users.Db.SaveChanges();

            Users.AllUsers.Remove(Users.SelectedUser);
        }

        public void LinkSubjects(object param)
        {
            Application.Current.MainWindow.DataContext = new LinkSubjects(Users.SelectedUser);
        }
        public bool IsSelectedUserProfessor(object o)
        {
            return Users.SelectedUser?.Type == UserType.Professor;
        }

        public void ChangeClass(object param)
        {
            Application.Current.MainWindow.DataContext = new ChangeClass(Users.Db.Students.First(s => s.UserId == Users.SelectedUser.UserId));
        }
        public bool IsSelectedUserStudent(object o)
        {
            return Users.SelectedUser?.Type == UserType.Student;
        }
    }
}
