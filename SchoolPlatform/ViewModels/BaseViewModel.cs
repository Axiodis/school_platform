﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using SchoolPlatform.Annotations;
using SchoolPlatform.Models;

namespace SchoolPlatform.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged, IDisposable
    {
        public static User LoggedUser { get; set; }

        public SchoolPlatformContext Db { get; } = new SchoolPlatformContext();

        public DateTime Now { get; set; }
        public int Semester { get; set; }

        public BaseViewModel()
        {
            Now = DateTime.Now;

            Semester = 1;

            if (Now.Month >= 3 && Now.Month <= 6)
            {
                Semester = 2;
            }

            if (Now.Month >= 7 && Now.Month <= 9)
            {
                Semester = 0;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Dispose()
        {
            Db.Dispose();
        }
    }
}
