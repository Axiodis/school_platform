﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using SchoolPlatform.Models;
using SchoolPlatform.ViewModels.Commands;

namespace SchoolPlatform.ViewModels.Professor
{
    public class AbsencesViewModel : BaseViewModel
    {
        private ObservableCollection<Absence> _absences;
        public ObservableCollection<Absence> Absences
        {
            get { return _absences; }
            set
            {
                _absences = value;
                OnPropertyChanged(nameof(Absences));
            }
        }

        public Absence SelectedAbsence { get; set; }

        public AbsencesViewModel()
        {
            Absences = new ObservableCollection<Absence>(Db.Absences.Where(a => a.UserId == LoggedUser.UserId).ToList());
        }

        private ICommand _backCommand;
        public ICommand BackCommand => _backCommand ?? (_backCommand = new RelayCommand(GoBack));

        public void GoBack(object param)
        {
            Application.Current.MainWindow.DataContext = new MainViewModel();
        }

        private ICommand _addAbsenceCommand;
        public ICommand AddAbsenceCommand => _addAbsenceCommand ?? (_addAbsenceCommand = new RelayCommand(AddAbsence));

        public void AddAbsence(object param)
        {
            Application.Current.MainWindow.DataContext = new AddAbsenceViewModel();
        }


        private ICommand _motivateCommand;
        public ICommand MotivateCommand => _motivateCommand ?? (_motivateCommand = new RelayCommand(Motivate,IsAbsenceSelected));

        public void Motivate(object param)
        {
            if (SelectedAbsence.IsNotMotivatable)
            {
                MessageBox.Show("You can't motivate this Absence!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Db.Absences.First(a => a.AbsenceId == SelectedAbsence.AbsenceId).IsMotivated = true;
            Db.SaveChanges();

            Absences = new ObservableCollection<Absence>(Db.Absences.Where(a => a.UserId == LoggedUser.UserId).ToList());

        }
        public bool IsAbsenceSelected(object param)
        {
            return SelectedAbsence != null;
        }
    }
}
