﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using SchoolPlatform.Models;
using SchoolPlatform.ViewModels.Commands;

namespace SchoolPlatform.ViewModels.Professor
{
    public class AddAbsenceViewModel : BaseViewModel
    {
        private Subject _selectedSubject;
        private SubjectClass _selectedClass;
        private ObservableCollection<SubjectClass> _classes;
        private ObservableCollection<Models.Student> _students;
        public ObservableCollection<Subject> Subjects { get; set; }

        public ObservableCollection<SubjectClass> Classes
        {
            get { return _classes; }
            set
            {
                _classes = value;
                OnPropertyChanged(nameof(Classes));
            }
        }

        public ObservableCollection<Models.Student> Students
        {
            get { return _students; }
            set
            {
                _students = value;
                OnPropertyChanged(nameof(Students));
            }
        }


        public Subject SelectedSubject
        {
            get { return _selectedSubject; }
            set
            {
                _selectedSubject = value;
                Classes = new ObservableCollection<SubjectClass>(Db.SubjectClasses.Where(c => c.SubjectId == SelectedSubject.SubjectId && c.UserId == LoggedUser.UserId).ToList());
            }
        }

        public SubjectClass SelectedClass
        {
            get { return _selectedClass; }
            set
            {
                _selectedClass = value;
                Students = _selectedClass == null ? null : new ObservableCollection<Models.Student>(SelectedClass.Class.Students.ToList());
            }
        }

        public Models.Student SelectedStudent { get; set; }
        public DateTime SelectedDate { get; set; }

        private bool? _isNotMotivatable;
        public bool? IsNotMotivatable
        {
            get { return _isNotMotivatable ?? false; }
            set { _isNotMotivatable = value; }
        }
        

        public AddAbsenceViewModel()
        {
            Subjects = new ObservableCollection<Subject>(Db.Users.First(u => u.UserId == LoggedUser.UserId).Subjects.ToList());
        }


        private ICommand _cancelCommand;
        public ICommand CancelCommand => _cancelCommand ?? (_cancelCommand = new RelayCommand(Cancel));

        public void Cancel(object param)
        {
            Application.Current.MainWindow.DataContext = new AbsencesViewModel();
        }


        private ICommand _addCommand;
        public ICommand AddCommand => _addCommand ?? (_addCommand = new RelayCommand(Add));

        public void Add(object param)
        {
            if (SelectedSubject == null || SelectedClass == null || SelectedStudent == null)
            {
                MessageBox.Show("You have to chose the student!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (Semester==0)
            {
                MessageBox.Show("You can't add marks in this period!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            Db.Absences.Add(new Absence
            {
                Date = Now,
                IsNotMotivatable = (bool)IsNotMotivatable,
                StudentId = SelectedStudent.StudentId,
                SubjectId = SelectedSubject.SubjectId,
                UserId = LoggedUser.UserId,
                IsMotivated = false,
                Semester = Semester
            });
            Db.SaveChanges();
            Application.Current.MainWindow.DataContext = new AbsencesViewModel();
        }
    }
}
