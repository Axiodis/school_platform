﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using SchoolPlatform.Models;
using SchoolPlatform.ViewModels.Commands;

namespace SchoolPlatform.ViewModels.Professor
{
    public class AddMarkViewModel : BaseViewModel
    {
        private Subject _selectedSubject;
        private SubjectClass _selectedClass;
        private ObservableCollection<SubjectClass> _classes;
        private ObservableCollection<Models.Student> _students;
        public ObservableCollection<Subject> Subjects { get; set; }

        public ObservableCollection<SubjectClass> Classes
        {
            get { return _classes; }
            set
            {
                _classes = value;
                OnPropertyChanged(nameof(Classes));
            }
        }

        public ObservableCollection<Models.Student> Students
        {
            get { return _students; }
            set
            {
                _students = value;
                OnPropertyChanged(nameof(Students));
            }
        }


        public Subject SelectedSubject
        {
            get { return _selectedSubject; }
            set
            {
                _selectedSubject = value;
                Classes = new ObservableCollection<SubjectClass>(Db.SubjectClasses.Where(c => c.SubjectId == SelectedSubject.SubjectId && c.UserId==LoggedUser.UserId).ToList());
            }
        }

        public SubjectClass SelectedClass
        {
            get { return _selectedClass; }
            set
            {
                _selectedClass = value;
                Students = _selectedClass == null ? null : new ObservableCollection<Models.Student>(SelectedClass.Class.Students.ToList());
            }
        }

        public Models.Student SelectedStudent { get; set; }

        private bool? _isTeza;
        public bool? IsTeza
        {
            get { return _isTeza ?? false; }
            set { _isTeza = value; }
        }

        public string MarkValue { get; set; }

        public AddMarkViewModel()
        {
            Subjects = new ObservableCollection<Subject>(Db.Users.First(u => u.UserId == LoggedUser.UserId).Subjects.ToList());
        }

        private ICommand _cancelCommand;
        public ICommand CancelCommand => _cancelCommand ?? (_cancelCommand = new RelayCommand(Cancel));

        public void Cancel(object param)
        {
            Application.Current.MainWindow.DataContext = new AbsencesViewModel();
        }


        private ICommand _addCommand;
        public ICommand AddCommand => _addCommand ?? (_addCommand = new RelayCommand(Add));

        public void Add(object param)
        {
            if (SelectedSubject == null || SelectedClass == null || SelectedStudent == null)
            {
                MessageBox.Show("You have to chose the student!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            int mark;

            try
            {
                mark = int.Parse(MarkValue);
                if (mark < 1 || mark > 10)
                {
                    MessageBox.Show("The mark must be between 1 and 10!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("The mark must be a number!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (Db.Averages.Any(a => a.StudentId == SelectedStudent.StudentId &&
                                       a.SubjectId == SelectedSubject.SubjectId && a.Year == Now.Year &&
                                       a.Semester == Semester))
            {
                MessageBox.Show("You can't add marks after you calculated the average!!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            if ((bool)IsTeza)
            {
                try
                {
                    var unused = Db.Marks.First(m => m.StudentId == SelectedStudent.StudentId &&
                                                     m.SubjectId == SelectedSubject.SubjectId && m.IsTeza);
                    MessageBox.Show("This student has a Teza at this Subject!", "Error!", MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    return;
                }
                catch (Exception)
                {
                    //ignored
                }
            }
            if (Semester == 0)
            {
                MessageBox.Show("You can't add marks in this period!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            Db.Marks.Add(new Mark
            {
                UserId = LoggedUser.UserId,
                StudentId = SelectedStudent.StudentId,
                SubjectId = SelectedSubject.SubjectId,
                Value = mark,
                Date = Now,
                IsTeza = (bool)IsTeza,
                Semester = Semester

            });
            Db.SaveChanges();
            Application.Current.MainWindow.DataContext = new MarksViewModel();
        }
    }
}
