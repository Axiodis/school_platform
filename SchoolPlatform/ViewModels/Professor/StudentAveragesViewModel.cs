﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using SchoolPlatform.Models;
using SchoolPlatform.ViewModels.Commands;

namespace SchoolPlatform.ViewModels.Professor
{
    public class StudentAveragesViewModel : BaseViewModel
    {
        public StudentAveragesViewModel()
        {
            
        }
        public StudentAveragesViewModel(Models.Student selectedStudent)
        {
            Student = selectedStudent;
            Averages = new ObservableCollection<Average>(Db.Averages.Where(a => a.StudentId==Student.StudentId && a.Semester == Semester && a.Year == Now.Year).ToList());

            int semesterAverage = 0;
            foreach (var avg in Averages)
            {
                semesterAverage += avg.Value;
            }

            SemesterAverage = (semesterAverage / Averages.Count).ToString();
        }

        public Models.Student Student { get; set; }
        public ObservableCollection<Average> Averages { get; set; }

        public string SemesterAverage { get; set; }



        private ICommand _backCommand;
        public ICommand BackCommand => _backCommand ?? (_backCommand = new RelayCommand(GoBack));

        public void GoBack(object param)
        {
            Application.Current.MainWindow.DataContext = new MainViewModel();
        }


    }
}
