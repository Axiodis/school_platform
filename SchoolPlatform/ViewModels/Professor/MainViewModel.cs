﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using SchoolPlatform.ViewModels.Admin;
using SchoolPlatform.ViewModels.Commands;

namespace SchoolPlatform.ViewModels.Professor
{
    public class MainViewModel : BaseViewModel
    {
        private ICommand _absencesCommand;
        public ICommand AbsencesCommand => _absencesCommand ?? (_absencesCommand = new RelayCommand(Absences));

        public void Absences(object param)
        {
            Application.Current.MainWindow.DataContext = new AbsencesViewModel();
        }


        private ICommand _marksCommand;
        public ICommand MarksCommand => _marksCommand ?? (_marksCommand = new RelayCommand(Marks));

        public void Marks(object param)
        {
            Application.Current.MainWindow.DataContext = new MarksViewModel();
        }


        private ICommand _classMasterCommand;
        public ICommand ClassMasterCommand => _classMasterCommand ?? (_classMasterCommand = new RelayCommand(ClassMaster));

        public void ClassMaster(object param)
        {
            Application.Current.MainWindow.DataContext = new ClassMasterViewModel();
        }


        private ICommand _averagesCommand;
        public ICommand AveragesCommand => _averagesCommand ?? (_averagesCommand = new RelayCommand(Averages));

        public void Averages(object param)
        {
            Application.Current.MainWindow.DataContext = new AveragesViewModel();
        }


        private ICommand _exitCommand;
        public ICommand ExitCommand => _exitCommand ?? (_exitCommand = new RelayCommand(Exit));

        public void Exit(object param)
        {
            LoggedUser = null;
            Application.Current.MainWindow.DataContext = new LogInViewModel();
        }
    }
}
