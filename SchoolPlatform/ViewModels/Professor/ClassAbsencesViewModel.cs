﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using SchoolPlatform.Models;
using SchoolPlatform.ViewModels.Commands;

namespace SchoolPlatform.ViewModels.Professor
{
    public class ClassAbsencesViewModel : BaseViewModel
    {
        public ObservableCollection<Absence> Absences { get; set; }

        public Models.Student Student { get; set; }

        public Absence SelectedAbsence { get; set; }

        public string TotalAbsences { get; set; }
        public string NotMotivatedAbsences { get; set; }

        public ClassAbsencesViewModel()
        {
            Class thisClass = Db.Classes.First(c => c.UserId == LoggedUser.UserId);

            Absences = new ObservableCollection<Absence>(Db.Absences.Join(
                    Db.Students,
                    abs => abs.StudentId,
                    std => std.StudentId,
                    (abs, std) => new {Absences = abs, Students = std})
                .Where(t => t.Students.Class.ClassId == thisClass.ClassId && t.Absences.Semester == Semester && t.Absences.Date.Year == Now.Year)
                .Select(t => t.Absences)
                .ToList());

            TotalAbsences = Absences.Count.ToString();
            NotMotivatedAbsences = Absences.Count(a => a.IsMotivated == false).ToString();
        }

        private ICommand _backCommand;
        public ICommand BackCommand => _backCommand ?? (_backCommand = new RelayCommand(GoBack));

        public void GoBack(object param)
        {
            Application.Current.MainWindow.DataContext = new MainViewModel();
        }

        private ICommand _motivateCommand;
        public ICommand MotivateCommand => _motivateCommand ?? (_motivateCommand = new RelayCommand(Motivate, IsAbsenceSelected));

        public void Motivate(object param)
        {
            if (SelectedAbsence.IsNotMotivatable)
            {
                MessageBox.Show("You can't motivate this Absence!", "Error!", MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }
            Absences.First(a => a.AbsenceId == SelectedAbsence.AbsenceId).IsMotivated = true;
            Db.SaveChanges();
            NotMotivatedAbsences = (int.Parse(NotMotivatedAbsences) - 1).ToString();
        }

        public bool IsAbsenceSelected(object param)
        {
            return SelectedAbsence != null;
        }
    }
}
