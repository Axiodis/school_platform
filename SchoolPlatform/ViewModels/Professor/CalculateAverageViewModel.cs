﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using SchoolPlatform.Models;
using SchoolPlatform.ViewModels.Commands;

namespace SchoolPlatform.ViewModels.Professor
{
    public class CalculateAverageViewModel :BaseViewModel
    {
        private Subject _selectedSubject;
        private SubjectClass _selectedClass;
        private ObservableCollection<SubjectClass> _classes;
        private ObservableCollection<Models.Student> _students;
        public ObservableCollection<Subject> Subjects { get; set; }

        public ObservableCollection<SubjectClass> Classes
        {
            get { return _classes; }
            set
            {
                _classes = value;
                OnPropertyChanged(nameof(Classes));
            }
        }

        public ObservableCollection<Models.Student> Students
        {
            get { return _students; }
            set
            {
                _students = value;
                OnPropertyChanged(nameof(Students));
            }
        }


        public Subject SelectedSubject
        {
            get { return _selectedSubject; }
            set
            {
                _selectedSubject = value;
                Classes = new ObservableCollection<SubjectClass>(Db.SubjectClasses.Where(c => c.SubjectId == SelectedSubject.SubjectId && c.UserId == LoggedUser.UserId).ToList());
            }
        }

        public SubjectClass SelectedClass
        {
            get { return _selectedClass; }
            set
            {
                _selectedClass = value;
                Students = _selectedClass == null ? null : new ObservableCollection<Models.Student>(SelectedClass.Class.Students.ToList());

                foreach (var student in SelectedClass.Class.Students.ToList())
                {
                    if (student.Averages.Count(a => a.SubjectId == SelectedSubject.SubjectId &&
                                                    a.Semester == Semester && a.Year == Now.Year) > 0)
                    {
                        Students?.Remove(student);
                    }
                }
            }
        }

        public Models.Student SelectedStudent { get; set; }
        public CalculateAverageViewModel()
        {
            Subjects = new ObservableCollection<Subject>(Db.Users.First(u => u.UserId == LoggedUser.UserId).Subjects.ToList());
        }

        private ICommand _cancelCommand;
        public ICommand CancelCommand => _cancelCommand ?? (_cancelCommand = new RelayCommand(Cancel));

        public void Cancel(object param)
        {
            Application.Current.MainWindow.DataContext = new AveragesViewModel();
        }


        private ICommand _calculateCommand;
        public ICommand CalculateCommand => _calculateCommand ?? (_calculateCommand = new RelayCommand(Calculate));

        public void Calculate(object param)
        {
            int avg=0;
            
            List<Mark> marks = Db.Marks.Where(m => m.StudentId == SelectedStudent.StudentId &&
                                             m.SubjectId == SelectedSubject.SubjectId && m.Semester == Semester &&
                                             m.Date.Year == Now.Year)
                .ToList();
            if (marks.Count<3)
            {
                MessageBox.Show("You can't calculate this average. Three marks are needed!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            foreach (Mark mark in marks)
            {
                avg += mark.Value;
            }

            avg = avg / marks.Count;

            if (SelectedClass.HasTeza)
            {
                try
                {
                    var teza = Db.Marks
                        .First(m => m.StudentId == SelectedStudent.StudentId &&
                                    m.SubjectId == SelectedSubject.SubjectId && m.Semester == Semester &&
                                    m.Date.Year == Now.Year && m.IsTeza);
                    avg += teza.Value;
                    avg /= 2;
                }
                catch (Exception)
                {
                    MessageBox.Show("You can't calculate this average. Teza is missing!", "Error!",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            Db.Averages.Add(new Average
            {
                Year = Now.Year,
                Semester = Semester,
                UserId = LoggedUser.UserId,
                StudentId = SelectedStudent.StudentId,
                SubjectId = SelectedSubject.SubjectId,
                Value = avg
            });
            Db.SaveChanges();
            Application.Current.MainWindow.DataContext = new AveragesViewModel();
        }
    }
}
