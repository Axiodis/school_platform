﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using SchoolPlatform.Models;
using SchoolPlatform.ViewModels.Commands;

namespace SchoolPlatform.ViewModels.Professor
{
    public class AveragesViewModel : BaseViewModel
    {
        public ObservableCollection<Average> Averages { get; set; }

        public AveragesViewModel()
        {
            Averages = new ObservableCollection<Average>(Db.Averages.Where(a => a.UserId == LoggedUser.UserId && a.Semester == Semester && a.Year == Now.Year).ToList());
        }

        private ICommand _backCommand;
        public ICommand BackCommand => _backCommand ?? (_backCommand = new RelayCommand(GoBack));

        public void GoBack(object param)
        {
            Application.Current.MainWindow.DataContext = new MainViewModel();
        }

        private ICommand _calculateCommand;
        public ICommand CalculateCommand => _calculateCommand ?? (_calculateCommand = new RelayCommand(Calculate));

        public void Calculate(object param)
        {
            Application.Current.MainWindow.DataContext = new CalculateAverageViewModel();
        }
    }
}
