﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using SchoolPlatform.Models;
using SchoolPlatform.ViewModels.Commands;

namespace SchoolPlatform.ViewModels.Professor
{
    public class MarksViewModel : BaseViewModel
    {
        public ObservableCollection<Mark> Marks { get; set; }

        public MarksViewModel()
        {
            Marks = new ObservableCollection<Mark>(Db.Marks.Where(m => m.UserId == LoggedUser.UserId).ToList());
        }

        private ICommand _backCommand;
        public ICommand BackCommand => _backCommand ?? (_backCommand = new RelayCommand(GoBack));

        public void GoBack(object param)
        {
            Application.Current.MainWindow.DataContext = new MainViewModel();
        }


        private ICommand _addMarkCommand;
        public ICommand AddMarkCommand => _addMarkCommand ?? (_addMarkCommand = new RelayCommand(AddMark));

        public void AddMark(object param)
        {
            Application.Current.MainWindow.DataContext = new AddMarkViewModel();
        }


        private ICommand _cancelMarkCommand;
        public ICommand CancelMarkCommand => _cancelMarkCommand ?? (_cancelMarkCommand = new RelayCommand(CancelMark));

        public void CancelMark(object param)
        {

        }
    }
}
