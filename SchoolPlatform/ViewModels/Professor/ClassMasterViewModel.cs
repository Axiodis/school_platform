﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using SchoolPlatform.Models;
using SchoolPlatform.ViewModels.Commands;

namespace SchoolPlatform.ViewModels.Professor
{
    public class ClassMasterViewModel : BaseViewModel
    {
        public ObservableCollection<Models.Student> Students { get; set; }
        public Models.Student SelectedStudent { get; set; }

        public ClassMasterViewModel()
        {
            Students = new ObservableCollection<Models.Student>(Db.Classes.First(c => c.UserId == LoggedUser.UserId).Students.ToList());
        }

        private ICommand _backCommand;
        public ICommand BackCommand => _backCommand ?? (_backCommand = new RelayCommand(GoBack));

        public void GoBack(object param)
        {
            Application.Current.MainWindow.DataContext = new MainViewModel();
        }


        private ICommand _studentAbsencesCommand;
        public ICommand StudentAbsencesCommand => _studentAbsencesCommand ?? (_studentAbsencesCommand = new RelayCommand(StudentAbsences, IsStudentSelected));

        public void StudentAbsences(object param)
        {
            Application.Current.MainWindow.DataContext = new StudentAbsencesViewModel(SelectedStudent);
        }


        private ICommand _classAbsencesCommand;
        public ICommand ClassAbsencesCommand => _classAbsencesCommand ?? (_classAbsencesCommand = new RelayCommand(ClassAbsencess));

        public void ClassAbsencess(object param)
        {
            Application.Current.MainWindow.DataContext = new ClassAbsencesViewModel();
        }

        private ICommand _studentAveragesCommand;
        public ICommand StudentAveragesCommand => _studentAveragesCommand ?? (_studentAveragesCommand = new RelayCommand(StudentAverages, IsStudentSelected));

        public void StudentAverages(object param)
        {
            Application.Current.MainWindow.DataContext = new StudentAveragesViewModel(SelectedStudent);
        }

        public bool IsStudentSelected(object param)
        {
            return SelectedStudent != null;
        }
    }
}
