﻿using System.Windows.Input;
using SchoolPlatform.ViewModels.Commands;
using SchoolPlatform.ViewModels.Operations.AdminOperations;

namespace SchoolPlatform.ViewModels.Admin
{
    public class MainViewModel : BaseViewModel
    {
        public AdminMainOperations AdminMainOperations { get; }

        public MainViewModel()
        {
            AdminMainOperations = new AdminMainOperations(this);
        }

        private ICommand _studentsCommand;
        public ICommand StudentsCommand => _studentsCommand ?? (_studentsCommand = new RelayCommand(AdminMainOperations.Students));

        private ICommand _classesCommand;
        public ICommand ClassesCommand => _classesCommand ?? (_classesCommand = new RelayCommand(AdminMainOperations.Classes));

        private ICommand _subjectsCommand;
        public ICommand SubjectsCommand => _subjectsCommand ?? (_subjectsCommand = new RelayCommand(AdminMainOperations.Subjects));

        private ICommand _subjectClassCommand;
        public ICommand SubjectsClassesCommand => _subjectClassCommand ?? (_subjectClassCommand = new RelayCommand(AdminMainOperations.SubjectsClasses));

        private ICommand _exitCommand;
        public ICommand ExitCommand => _exitCommand ?? (_exitCommand = new RelayCommand(AdminMainOperations.Exit));
    }
}