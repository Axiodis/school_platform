﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using SchoolPlatform.Models;
using SchoolPlatform.ViewModels.Commands;

namespace SchoolPlatform.ViewModels.Admin
{
    public class Classes : BaseViewModel
    {
        public ObservableCollection<Class> AllClasses { get; set; }

        public Class SelectedClass { get; set; }
        public Classes()
        {
            AllClasses = new ObservableCollection<Class>(Db.Classes.ToList());
        }

        private ICommand _addClassCommand;
        public ICommand AddClassCommand => _addClassCommand ?? (_addClassCommand = new RelayCommand(AddClass));

        public void AddClass(object param)
        {
            Application.Current.MainWindow.DataContext = new AddClass();
        }


        private ICommand _saveChangesCommand;
        public ICommand SaveChangesCommand => _saveChangesCommand ?? (_saveChangesCommand = new RelayCommand(SaveChanges));

        public void SaveChanges(object param)
        {
            Db.SaveChanges();
        }

        private ICommand _backCommand;
        public ICommand BackCommand => _backCommand ?? (_backCommand = new RelayCommand(GoBack));

        public void GoBack(object param)
        {
            Application.Current.MainWindow.DataContext = new MainViewModel();
        }

        private ICommand _deleteClassCommand;
        public ICommand DeleteClassCommand => _deleteClassCommand ?? (_deleteClassCommand = new RelayCommand(DeleteClass));
        
        public void DeleteClass(object param)
        {
            if (SelectedClass == null)
            {
                MessageBox.Show("You have to chose a Class!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Db.Classes.Remove(SelectedClass);
            AllClasses.Remove(SelectedClass);
            Db.SaveChanges();
        }

        private ICommand _changeClassMasterCommand;
        public ICommand ChangeClassMasterCommand => _changeClassMasterCommand ?? (_changeClassMasterCommand = new RelayCommand(ChangeClassMaster));

        public void ChangeClassMaster(object param)
        {
            if (SelectedClass == null)
            {
                MessageBox.Show("You have to chose a Class!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Application.Current.MainWindow.DataContext = new ChangeClassMaster(SelectedClass);
        }
    }
}
