﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using SchoolPlatform.Models;
using SchoolPlatform.ViewModels.Commands;

namespace SchoolPlatform.ViewModels.Admin
{
    public class LinkSubjects : BaseViewModel
    {
        public User SelectedProfessor { get; set; }

        public ObservableCollection<Subject> CurrentSubjects { get; set; }

        public ObservableCollection<Subject> PossibleSubjects { get; set; }

        public Subject SelectedPossibleSubject { get; set; }
        public Subject SelectedCurrentSubject { get; set; }
        public LinkSubjects(User selectedProfessor)
        {
            SelectedProfessor = selectedProfessor;
            CurrentSubjects = new ObservableCollection<Subject>(SelectedProfessor.Subjects.ToList());
            PossibleSubjects = new ObservableCollection<Subject>();
            
            foreach (var subject in Db.Subjects)
            {
                try
                {
                    Subject firstOrDefault = SelectedProfessor.Subjects.First(s => s.SubjectId == subject.SubjectId);
                }
                catch (Exception)
                {
                    PossibleSubjects.Add(subject);
                }
            }
        }

        private ICommand _backCommand;
        public ICommand BackCommand => _backCommand ?? (_backCommand = new RelayCommand(GoBack));

        public void GoBack(object param)
        {
            Application.Current.MainWindow.DataContext = new Users();
        }

        private ICommand _linkSubjectCommand;
        public ICommand LinkSubjectCommand => _linkSubjectCommand ?? (_linkSubjectCommand = new RelayCommand(LinkSubject, IsSelectedPossibleSubject));

        public void LinkSubject(object param)
        {
            Db.Users.Find(SelectedProfessor.UserId)?.Subjects.Add(SelectedPossibleSubject);
            Db.SaveChanges();
            CurrentSubjects.Add(SelectedPossibleSubject);
            PossibleSubjects.Remove(SelectedPossibleSubject);
        }

        private bool IsSelectedPossibleSubject(object o)
        {
            return SelectedPossibleSubject != null;
        }


        private ICommand _unlinkSubjecCommand;
        public ICommand UnlinkSubjectCommand => _unlinkSubjecCommand ?? (_unlinkSubjecCommand = new RelayCommand(UnlinkSubject, IsSelectedCurrentSubject));

        public void UnlinkSubject(object param)
        {
            var subject = Db.Subjects.Find(SelectedCurrentSubject.SubjectId);
            var prof = Db.Users.Find(SelectedProfessor.UserId);

            Db.Entry(prof).Collection("Subjects").Load();
            prof?.Subjects.Remove(subject);
            Db.SaveChanges();

            CurrentSubjects.Remove(SelectedCurrentSubject);
            PossibleSubjects.Add(SelectedCurrentSubject);
        }

        private bool IsSelectedCurrentSubject(object o)
        {
            return SelectedCurrentSubject != null;
        }
    }
}
