﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using SchoolPlatform.Models;
using SchoolPlatform.ViewModels.Commands;

namespace SchoolPlatform.ViewModels.Admin
{
    public class Subjects : BaseViewModel
    {
        public ObservableCollection<Subject> AllSubjects { get; set; }

        public Subject SelectedSubject { get; set; }

        public Subjects()
        {
            AllSubjects = new ObservableCollection<Subject>(Db.Subjects.ToList());
        }

        private ICommand _addSubjectCommand;
        public ICommand AddSubjectCommand => _addSubjectCommand ?? (_addSubjectCommand = new RelayCommand(AddSubject));

        public void AddSubject(object param)
        {
            Application.Current.MainWindow.DataContext = new AddSubject();
        }


        private ICommand _saveChangesCommand;
        public ICommand SaveChangesCommand => _saveChangesCommand ??
                                              (_saveChangesCommand = new RelayCommand(SaveChanges));

        public void SaveChanges(object param)
        {
            Db.SaveChanges();
        }

        private ICommand _backCommand;
        public ICommand BackCommand => _backCommand ?? (_backCommand = new RelayCommand(GoBack));

        public void GoBack(object param)
        {
            Application.Current.MainWindow.DataContext = new MainViewModel();

        }
    }
}
