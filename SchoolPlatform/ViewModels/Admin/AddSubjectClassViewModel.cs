﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using SchoolPlatform.Models;
using SchoolPlatform.ViewModels.Commands;

namespace SchoolPlatform.ViewModels.Admin
{
    public class AddSubjectClassViewModel : BaseViewModel
    {

        public ObservableCollection<Class> AllClasses { get; set; }

        private ObservableCollection<User> _professors;

        public ObservableCollection<User> Professors
        {
            get { return _professors; }
            set
            {
                _professors = value;
                OnPropertyChanged(nameof(Professors));
            }
        }

        private ObservableCollection<Subject> _subjects;
        public ObservableCollection<Subject> Subjects
        {
            get { return _subjects; }
            set
            {
                _subjects = value;
                OnPropertyChanged(nameof(Subjects));
            }
        }

        private Class _selectedClass;
        public Class SelectedClass
        {
            get { return _selectedClass; }
            set
            {
                _selectedClass = value;
                Subjects = new ObservableCollection<Subject>(Db.Subjects
                    .GroupJoin(
                        Db.SubjectClasses,
                        subj => subj.SubjectId,
                        subjClass => subjClass.SubjectId,
                        (subj, subjClass) => new { Subject = subj, SubjectClass = subjClass }
                    )
                    .SelectMany(
                        subj => subj.SubjectClass.DefaultIfEmpty(),
                        (subj, subjClass) => new { Subject = subj, SubjectClass = subjClass }
                    )
                    .Where(subj => subj.SubjectClass.Class.ClassId != SelectedClass.ClassId)
                    .Select(subj => subj.Subject.Subject)
                    .ToList());
            }
        }

        private Subject _selectedSubject;

        public Subject SelectedSubject
        {
            get { return _selectedSubject; }
            set
            {
                _selectedSubject = value;
                Professors = new ObservableCollection<User>(SelectedSubject.Professors.ToList());
            }
        }

        public User SelectedProfessor{ get; set; }

        private bool? _hasTeza;
        public bool? HasTeza
        {
            get { return _hasTeza ?? false; }
            set
            {
                _hasTeza = value;
            }
        }

        public AddSubjectClassViewModel()
        {
            AllClasses = new ObservableCollection<Class>(Db.Classes.ToList());
        }


        private ICommand _cencelCommand;
        public ICommand CancelCommand => _cencelCommand ?? (_cencelCommand = new RelayCommand(Cancel));
        public void Cancel(object param)
        {
            Application.Current.MainWindow.DataContext = new SubjectClassViewModel();
        }


        private ICommand _addCommand;
        public ICommand AddCommand => _addCommand ?? (_addCommand = new RelayCommand(Add));
        public void Add(object param)
        {
            Db.SubjectClasses.Add(new SubjectClass
            {
                UserId = SelectedProfessor.UserId,
                ClassId = SelectedClass.ClassId,
                SubjectId = SelectedSubject.SubjectId,
                HasTeza = (bool) HasTeza
            });
            Db.SaveChanges();
            Application.Current.MainWindow.DataContext = new SubjectClassViewModel();
        }
    }
}
