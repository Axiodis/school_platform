﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using SchoolPlatform.Models;
using SchoolPlatform.ViewModels.Commands;

namespace SchoolPlatform.ViewModels.Admin
{
    public class AddUser : BaseViewModel
    {
        private UserType _type;
        private Visibility _isStudent = Visibility.Visible;

        public string Name { get; set; }

        public AddUser()
        {
            AllClasses = new ObservableCollection<Class>(Db.Classes.ToList());
        }

        public ObservableCollection<Class> AllClasses { get; set; }

        public Class SelectedClass { get; set; }

        public UserType Type
        {
            get { return _type; }
            set
            {
                IsStudent = value == UserType.Student ? Visibility.Visible : Visibility.Hidden;
                _type = value;
            }
        }

        public Visibility IsStudent
        {
            get { return _isStudent; }
            set
            {
                _isStudent = value;
                OnPropertyChanged(nameof(IsStudent));
            }
        }

        private ICommand _cancelCommand;
        public ICommand CancelCommand => _cancelCommand ?? (_cancelCommand = new RelayCommand(Cancel));

        public void Cancel(object param)
        {
            Application.Current.MainWindow.DataContext = new Users();
        }


        private ICommand _addCommand;
        
        public ICommand AddCommand => _addCommand ?? (_addCommand = new RelayCommand(Add));

        public void Add(object param)
        {

            int nrOfUsers = Db.Users.Count(u => u.Name.Equals(Name));

            User newUser = new User
            {
                Name = Name,
                Type = Type,
                Password = "1234",
                Username = Name.Replace(' ', '-').ToLower() + '-' + nrOfUsers
            };

            if (Type == UserType.Student)
            {
                if (SelectedClass == null)
                {
                    MessageBox.Show("Chose a class for the student!", "Error!", MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    return;
                }
                Models.Student newStudent = new Models.Student { ClassId = SelectedClass.ClassId, UserId = newUser.UserId };
                Db.Students.Add(newStudent);
            }

            Db.Users.Add(newUser);
            Db.SaveChanges();
            Application.Current.MainWindow.DataContext = new Users();
        }
    }
}
