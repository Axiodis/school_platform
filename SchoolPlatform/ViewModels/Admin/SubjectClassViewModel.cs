﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using SchoolPlatform.Models;
using SchoolPlatform.ViewModels.Commands;

namespace SchoolPlatform.ViewModels.Admin
{
    public class SubjectClassViewModel : BaseViewModel
    {
        public SubjectClass SelectedSubjectClass { get; set; }

        public ObservableCollection<SubjectClass> AllSubjectClasses { get; set; }
        public SubjectClassViewModel()
        {
            AllSubjectClasses = new ObservableCollection<SubjectClass>(Db.SubjectClasses.ToList());
        }

        private ICommand _backCommand;
        public ICommand BackCommand => _backCommand ?? (_backCommand = new RelayCommand(GoBack));
        public void GoBack(object param)
        {
            Application.Current.MainWindow.DataContext = new MainViewModel();
        }


        private ICommand _addSubjectClassCommand;
        public ICommand AddSubjectClassCommand => _addSubjectClassCommand ?? (_addSubjectClassCommand = new RelayCommand(AddSubjectClass));
        public void AddSubjectClass(object param)
        {
            Application.Current.MainWindow.DataContext = new AddSubjectClassViewModel();
        }


        private ICommand _removeSubjectClassCommand;
        public ICommand RemoveSubjectClassCommand => _removeSubjectClassCommand ?? (_removeSubjectClassCommand = new RelayCommand(RemoveSubjectClass, IsSubjectClassSelected));
        public void RemoveSubjectClass(object param)
        {
            SubjectClass subjectClass = Db.SubjectClasses.Find(SelectedSubjectClass.SubjectClassId);
            Db.SubjectClasses.Remove(subjectClass);
            Db.SaveChanges();
            AllSubjectClasses.Remove(subjectClass);
        }

        public bool IsSubjectClassSelected(object param)
        {
            return SelectedSubjectClass != null;
        }
    }
}
