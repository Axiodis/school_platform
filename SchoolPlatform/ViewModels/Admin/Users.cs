﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using SchoolPlatform.Models;
using SchoolPlatform.ViewModels.Commands;
using SchoolPlatform.ViewModels.Operations.AdminOperations;

namespace SchoolPlatform.ViewModels.Admin
{
    public class Users : BaseViewModel
    {
        public ObservableCollection<User> AllUsers { get; set; }
        public AdminUsersOperations AdminUsersOperations { get; }

        public User SelectedUser { get; set; }

        public Users()
        {
            AdminUsersOperations = new AdminUsersOperations(this);
            AllUsers = new ObservableCollection<User>(Db.Users.ToList());
        }

        private ICommand _addUserCommand;
        public ICommand AddUserCommand => _addUserCommand ?? (_addUserCommand = new RelayCommand(AdminUsersOperations.AddUser));

        private ICommand _goBackCommand;
        public ICommand GoBackCommand => _goBackCommand ?? (_goBackCommand = new RelayCommand(AdminUsersOperations.GoBack));

        private ICommand _saveChangesCommand;
        public ICommand SaveChangesCommand => _saveChangesCommand ?? (_saveChangesCommand = new RelayCommand(AdminUsersOperations.SaveChanges));

        private ICommand _deleteUserCommand;
        public ICommand DeleteUserCommand => _deleteUserCommand ?? (_deleteUserCommand = new RelayCommand(AdminUsersOperations.Delete));

        private ICommand _linkSubjectCommand;
        public ICommand LinkSubjectCommand => _linkSubjectCommand ?? (_linkSubjectCommand = new RelayCommand(AdminUsersOperations.LinkSubjects, AdminUsersOperations.IsSelectedUserProfessor));

        private ICommand _changeClassCommand;
        public ICommand ChangeClassCommand => _changeClassCommand ?? (_changeClassCommand = new RelayCommand(AdminUsersOperations.ChangeClass, AdminUsersOperations.IsSelectedUserStudent));

    }
}
