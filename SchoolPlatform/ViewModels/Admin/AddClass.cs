﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using SchoolPlatform.Models;
using SchoolPlatform.ViewModels.Commands;

namespace SchoolPlatform.ViewModels.Admin
{
    public class AddClass : BaseViewModel
    {
        public string Name { get; set; }

        public List<User> Professors { get; set; }

        public User SelectedProfessor { get; set; }

        public AddClass()
        {
            Professors = Db.Users
                .GroupJoin(
                    Db.Classes,
                    user => user.UserId,
                    clas => clas.UserId,
                    (user, clas) => new { Users = user, Classes = clas }
                )
                .SelectMany(
                    usr => usr.Classes.DefaultIfEmpty(),
                    (usr, clas) => new { Users = usr, Classes = clas }
                )
                .Where(usr => usr.Users.Users.Type == UserType.Professor && usr.Classes.Name == null)
                .Select(usr => usr.Users.Users)
                .ToList();
        }

        private ICommand _addCommand;
        public ICommand AddCommand => _addCommand ?? (_addCommand = new RelayCommand(Add));

        public void Add(object param)
        {
            if (SelectedProfessor == null)
            {
                MessageBox.Show("You have to chose a Class Master!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (Db.Classes.Count(c => c.Name.Equals(Name)) > 0)
            {
                MessageBox.Show("This class already exists!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                Class studentsClass = new Class { Name = Name, UserId = SelectedProfessor.UserId };
                Db.Classes.Add(studentsClass);
                Db.SaveChanges();
                Application.Current.MainWindow.DataContext = new Classes();
            }
        }

        private ICommand _cancelCommand;
        public ICommand CancelCommand => _cancelCommand ?? (_cancelCommand = new RelayCommand(Cancel));

        public void Cancel(object param)
        {
            Application.Current.MainWindow.DataContext = new Classes();
        }
    }
}
