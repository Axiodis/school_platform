﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using SchoolPlatform.Models;
using SchoolPlatform.ViewModels.Commands;

namespace SchoolPlatform.ViewModels.Admin
{
    public class ChangeClass : BaseViewModel
    {
        private string _currentClassName;

        public string CurrentClassName
        {
            get { return _currentClassName; }
            set
            {
                _currentClassName = value;
                OnPropertyChanged(nameof(CurrentClassName));
            }
        }

        public Models.Student Student { get; set; }

        public ObservableCollection<Class> Classes { get; set; }

        public Class SelectedClass { get; set; }

        public ChangeClass(Models.Student student)
        {
            Student = student;
            CurrentClassName = Student.Class.Name;
            Classes = new ObservableCollection<Class>(Db.Classes.Where(c => c.ClassId!=Student.Class.ClassId).ToList());
        }

        private ICommand _cancelCommand;
        public ICommand CancelCommand => _cancelCommand ?? (_cancelCommand = new RelayCommand(Cancel));

        public void Cancel(object param)
        {
            Application.Current.MainWindow.DataContext = new Users();
        }

        private ICommand _changeCommand;
        public ICommand ChangeCommand => _changeCommand ?? (_changeCommand = new RelayCommand(Change,IsClassSelected));
        
        public void Change(object param)
        {
            Classes.Add(Student.Class);

            CurrentClassName = SelectedClass.Name;

            Student.Class = SelectedClass;
            Db.Students.Find(Student.StudentId).Class = SelectedClass;
            Db.SaveChanges();
            
            Classes.Remove(SelectedClass);
        }

        private bool IsClassSelected(object o)
        {
            return SelectedClass != null;
        }
    }
}
