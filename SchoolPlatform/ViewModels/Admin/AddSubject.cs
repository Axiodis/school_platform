﻿using System.Linq;
using System.Windows;
using System.Windows.Input;
using SchoolPlatform.Models;
using SchoolPlatform.ViewModels.Commands;

namespace SchoolPlatform.ViewModels.Admin
{
    public class AddSubject : BaseViewModel
    {
        public string Name { get; set; }

        private ICommand _addCommand;
        public ICommand AddCommand => _addCommand ?? (_addCommand = new RelayCommand(Add));

        public void Add(object param)
        {
            if (Db.Subjects.Count(c => c.Name.Equals(Name)) > 0)
            {
                MessageBox.Show("This class already exists!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                Subject subject = new Subject { Name = Name };
                Db.Subjects.Add(subject);
                Db.SaveChanges();
                Application.Current.MainWindow.DataContext = new Subjects();
            }
        }

        private ICommand _cancelCommand;
        public ICommand CancelCommand => _cancelCommand ?? (_cancelCommand = new RelayCommand(Cancel));

        public void Cancel(object param)
        {
            Application.Current.MainWindow.DataContext = new Subjects();
        }
    }
}
