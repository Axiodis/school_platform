﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using SchoolPlatform.Models;
using SchoolPlatform.ViewModels.Commands;

namespace SchoolPlatform.ViewModels.Admin
{
    public class ChangeClassMaster : BaseViewModel
    {
        public List<User> Professors { get; set; }

        public User SelectedProfessor { get; set; }

        public Class ClassToChange { get; set; }
        public ChangeClassMaster(Class classToChange)
        {
            ClassToChange = classToChange;

            Professors = Db.Users
                .GroupJoin(
                    Db.Classes,
                    user => user.UserId,
                    clas => clas.UserId,
                    (user, clas) => new { Users = user, Classes = clas }
                )
                .SelectMany(
                    usr => usr.Classes.DefaultIfEmpty(),
                    (usr, clas) => new { Users = usr, Classes = clas }
                )
                .Where(usr => usr.Users.Users.Type == UserType.Professor && usr.Classes.Name == null)
                .Select(usr => usr.Users.Users)
                .ToList();
        }

        private ICommand _changeCommand;
        public ICommand ChangeCommand => _changeCommand ?? (_changeCommand = new RelayCommand(Change));

        public void Change(object param)
        {
            if (SelectedProfessor == null)
            {
                MessageBox.Show("You have to chose a Class Master!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Class classToChange = Db.Classes.Find(ClassToChange.ClassId);
            if (classToChange != null) classToChange.ClassMaster = SelectedProfessor;
            Db.SaveChanges();
            Application.Current.MainWindow.DataContext = new Classes();
        }

        private ICommand _cancelCommand;
        public ICommand CancelCommand => _cancelCommand ?? (_cancelCommand = new RelayCommand(Cancel));

        public void Cancel(object param)
        {
            Application.Current.MainWindow.DataContext = new Classes();
        }
    }
}
