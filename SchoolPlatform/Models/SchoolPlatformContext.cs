﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolPlatform.Models
{
    public class SchoolPlatformContext : DbContext
    {
        public SchoolPlatformContext() : base("Data Source=DESKTOP-JDDVHL1;Initial Catalog=SchoolPlatform_db;Persist Security Info=True;User ID=sa;Password=root")
        {
            
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Class> Classes { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<SubjectClass> SubjectClasses { get; set; }
        public DbSet<Mark> Marks { get; set; }
        public DbSet<Absence> Absences { get; set; }

        public DbSet<Average> Averages { get; set; }
    }
}
