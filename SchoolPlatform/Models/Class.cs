﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolPlatform.Models
{
    public class Class
    {
        public int ClassId { get; set; }

        [Required]
        public string Name { get; set; }

        [Index(IsUnique = true)]
        [Required]
        public int UserId { get; set; }

        public virtual ICollection<Student> Students { get; set; }

        public virtual ICollection<SubjectClass> SubjectClasses { get; set; }

        public virtual User ClassMaster { get; set; }
    }
}