﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolPlatform.Models
{
    public class Student
    {
        public int StudentId{ get; set; }

        [Index(IsUnique = true)]
        [Required]
        public int UserId { get; set; }
        [Required]
        public int ClassId { get; set; }
        public virtual User User { get; set; }
        public virtual Class Class { get; set; }
        public virtual ICollection<Average> Averages { get; set; }
    }
}
