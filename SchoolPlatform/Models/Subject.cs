﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolPlatform.Models
{
    public class Subject
    {
        public int SubjectId { get; set; }
        
        [Required]
        public string Name { get; set; }

        public virtual ICollection<User> Professors { get; set; }
    }
}
