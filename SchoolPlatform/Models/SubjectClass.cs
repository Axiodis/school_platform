﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolPlatform.Models
{
    public class SubjectClass
    {
        public int SubjectClassId { get; set; }

        [Required]
        public int UserId { get; set; }
        
        [Required]
        public int SubjectId { get; set; }

        [Required]
        public int ClassId { get; set; }

        public bool HasTeza { get; set; }
       
        public virtual User Professor { get; set; }

        public virtual Subject Subject { get; set; }

        public virtual Class Class { get; set; }
    }
}
