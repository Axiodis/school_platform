﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolPlatform.Models
{
    public class Mark
    {
        public int MarkId { get; set; }

        public int Value { get; set; }

        public DateTime Date { get; set; }

        public int Semester { get; set; }

        public bool IsTeza { get; set; }

        public int StudentId { get; set; }

        public int SubjectId { get; set; }

        public int UserId { get; set; }


        public virtual Student Student { get; set; }
        public virtual Subject Subject { get; set; }
        public virtual User Professor { get; set; }
    }
}
